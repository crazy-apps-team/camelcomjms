package com.projetos.exemplo.diego.CamelComJms.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsMessage;
import org.springframework.stereotype.Component;

@Component
public class CamelRoutes extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		String fila = "fila";
		
		from("direct:firstRoute").log("Camel body: ${body}").to("activemq:" + fila);
		
		from("activemq:" + fila).process(ex -> {
			JmsMessage in = ex.getIn(JmsMessage.class);
			System.out.println("Mensagem da fila : " + fila + " : " + in.getBody());
		});
		
	}
	
}
