package com.projetos.exemplo.diego.CamelComJms.controllers;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@ImportResource({ "classpath:spring/*.xml" })
public class CamelController {
	
	@Autowired
	ProducerTemplate producerTemplate;
	
	@RequestMapping(value ="/")
	public void startCamel() {

		producerTemplate.sendBody("direct:firstRoute", "Chamando via spring boot rest Controller");
		
	}

}
