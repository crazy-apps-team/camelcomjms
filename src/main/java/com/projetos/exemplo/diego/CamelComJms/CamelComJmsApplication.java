package com.projetos.exemplo.diego.CamelComJms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
public class CamelComJmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelComJmsApplication.class, args);
	}
}